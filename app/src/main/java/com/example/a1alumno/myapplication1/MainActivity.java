package com.example.a1alumno.myapplication1;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import static android.view.View.*;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Button button = (Button) findViewById(R.id.btn_helloworld);
       button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
               /* Context context = getApplicationContext();
                CharSequence text = "Hello toast!";
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();*/

                Toast toast = Toast.makeText(getApplicationContext(), "Hello toast!rrrrrr", Toast.LENGTH_SHORT);
                toast.show();
            }
        });

    }


}
